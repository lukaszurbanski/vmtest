module.exports = function(config){
  config.set({

    basePath : './',

    files : [

      './bower_components/angular/angular.js',
        './bower_components/angular-mocks/angular-mocks.js',
        './bower_components/angular-cookies/angular-cookies.js',
        './bower_components/angular-route/angular-route.js',
        './bower_components/angular-material/angular-material.js',
        './bower_components/angular-animate/angular-animate.js',
        './bower_components/angular-aria/angular-aria.js',
        './app/app.js',
        './app/app.config.js',
        './app/*.js',
        './app/view2/test/*.test.js',
        './app/view*/*.js',
        './app/test/*.test.js'

    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-safari-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
