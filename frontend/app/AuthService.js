app.service('AuthService', function () {
    var currentUser;
    var usersList = [];

    return {
        login: function (user) {
            currentUser = user;

        },
        currentUser: function () {
            return currentUser;
        },
        pushUsers: function (tab) {
            usersList = tab;

        },
        getUsers: function () {
            return usersList;
        },
        pushUser: function (user){
            usersList.push(user);
        }

    };
});

