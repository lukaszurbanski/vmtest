/**
 * Created by lukaszurbanski on 13.03.15.
 */
app.factory('dataFactory', ['$http', function ($http) {

    var urlBase = '../../backend/web/app_dev.php/';
    var dataFactory = {};
dataFactory.returnUrlBase = function(){
    return urlBase;
};
    dataFactory.getUsers = function (){
        return $http.get(urlBase + 'getUsers');
    };
    dataFactory.sendList = function (newList) {
        return $http.get(urlBase + 'addList/' + newList.listname + '/0/' + newList.whos);
    };
    dataFactory.sendItem = function (newItem) {
        return $http.get(urlBase + 'addProduct/' + newItem.productname + '/' + newItem.priority + '/' + newItem.listid);
    };
    dataFactory.delList = function (id) {
        return $http.get(urlBase + 'delList/' + id);
    };
    dataFactory.delProduct = function (id) {
        return $http.get(urlBase + 'delProduct/' + id);
    };
    dataFactory.getProducts  = function (list){
        return $http.get(urlBase + 'getProd/' + list);
    };
    dataFactory.getLists  = function (user){
        return $http.get(urlBase + 'getLists/' + user.id);
    };
    dataFactory.addUser = function (user) {
        return $http.get(urlBase + 'addUser/' + user.usenname + '/' + user.password + '?url=' + user.avatarurl);
    };
    dataFactory.delUser = function (id) {
        return $http.get(urlBase + 'delUser/' + id);
    };

    return dataFactory;
}]);
