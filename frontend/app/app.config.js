/**
 * Created by lukaszurbanski on 17.03.15.
 */

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/main', {
        templateUrl: 'view2/view2.html',
        controller: 'View2Ctrl'
    });
    $routeProvider.when('/login', {
        templateUrl: 'view1/view1.html',
        controller: 'Users'
    });

    $routeProvider.otherwise({redirectTo: '/login'});
}]);
