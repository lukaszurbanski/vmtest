'use strict';


app.controller('View2Ctrl', ['$scope', 'AuthService', 'dataFactory', '$mdDialog', '$location', '$mdBottomSheet', function ($scope, AuthService, dataFactory, $mdDialog, $location) {

    if (!AuthService.currentUser()) {
        $location.path("/login");

    }
    else {
        $scope.user = AuthService.currentUser();

    }
    $scope.addItem = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'view2/additem.html',
            targetEvent: ev
        })
            .then(function (answer) {
                var newItem = {};
                newItem.productname = answer.productname;
                newItem.priority = answer.priority;
                newItem.listid = $scope.selected;

                dataFactory.sendItem(newItem)
                    .success(function (response) {
                        newItem.id = response.id
                        $scope.Items.push(newItem);


                    });
            },
            function () {
            });
    };
    $scope.UserManagment = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'view2/users.html',
            targetEvent: ev
        })
            .then(function (answer) {

            },
            function () {
            });
    };
    $scope.addList = function (ev) {

        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'view2/addList.html',
            targetEvent: ev
        })
            .then(function (answer) {
                var newList = {};
                newList.listname = answer.listname;
                newList.itemscount = 0;
                if (answer.forall) {
                    newList.whos = "*";

                }
                else {
                    newList.whos = $scope.user.id;

                }
                dataFactory.sendList(newList)
                    .success(function (response) {
                        newList.id = response.id
                        $scope.Lists.push(newList);


                    });


            },
            function () {
                //$scope.cancel;
            });
    };




}]);

