/**
 * Created by lukaszurbanski on 16.03.15.
 */
app.filter('signs', function () {
    return function (input) {
        var text = "";
        for (var i = 0; i < input; i++) {
            text += "!";
        }
        return text;
    }
});