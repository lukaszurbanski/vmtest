/**
 * Created by lukaszurbanski on 16.03.15.
 */
function DialogController($scope, $mdDialog, AuthService, dataFactory,$location) {
    $scope.pod = {};
    $scope.users = AuthService.getUsers();
    $scope.showAvatar = false;
    $scope.status = 0;
    $scope.hide = function () {

        $mdDialog.hide();
        $scope.firstuser = 1;


    };
    $scope.cancel = function () {

        $mdDialog.cancel();
        $scope.firstuser = 1;


    };
    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
        $scope.firstuser = 1;


    };
    $scope.isImage = function (src) {

        var img = new Image();
        img.onLoad = function () {
            $scope.showAvatar = true;
            $scope.$digest();
        };
        img.onError = function () {
            $scope.showAvatar = false;
            $scope.$digest();

        };
        img.src = src;


    };
    $scope.isUndefined = function (thing) {
        return ( thing === "undefined" );
    };
    $scope.adduser = function (answer) {
        var newUser = {};
        newUser.usenname = answer.name;
        newUser.password = answer.pin;
        newUser.avatarurl = answer.avatar;

        dataFactory.addUser(newUser)
            .success(function () {

                AuthService.pushUser(newUser);
                AuthService.login(newUser);

            });
        $mdDialog.hide();
        $location.path("/main");


    };
    $scope.checkPin = function (pin1, pin2) {
        if (pin1 == pin2) {
            $scope.status = 1;
        }
        else {
            $scope.status = 0;

        }
    }


}