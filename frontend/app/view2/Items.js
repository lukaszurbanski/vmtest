/**
 * Created by lukaszurbanski on 25.02.15.
 */
'use strict';
app.controller('Items', function ($scope, $http, $filter, dataFactory) {


        var orderBy = $filter('orderBy');
        $scope.Items = orderBy($scope.Items, 'priority', false);
    $scope.editItems = false;
    $scope.delItemsTigger = function(){
        $scope.editItems = !$scope.editItems;
    };
    $scope.delItem = function (id, index){
        dataFactory.delProduct(id).success(function(){
            $scope.Items.splice(index, 1);
        });
    };

});
app.directive('items', function () {
    return {
        restrict: 'E',
        templateUrl: 'view2/Items.html',
        controller: 'Items'
    };
});

