/**
 * Created by lukaszurbanski on 16.03.15.
 */
describe('filter', function() {

    beforeEach(function(){
        module('myApp');
        it('has a signs filter', inject(function($filter) {
            expect($filter('signs')).not.toBeNull();
        }));
    });

    describe('signs', function() {

        it('should convert int values to int*"!"',
            inject(function(signsFilter) {
                expect(signsFilter(4)).toBe('!!!!');
                expect(signsFilter(10)).toBe('!!!!!!!!!!');
            }));
    });
});