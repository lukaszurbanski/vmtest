/**
 * Created by lukaszurbanski on 25.02.15.
 */
'use strict';
app.controller('Lists', function ($scope, $http, dataFactory ) {
    $scope.Lists = [];
    $scope.selected;
    $scope.editList = false;
    $scope.delList = function (id, index){
        dataFactory.delList(id).success(function(){
            $scope.Lists.splice(index, 1);
        });
    };

    $scope.delListTigger = function(){
        $scope.editList = !$scope.editList;
    };
    $scope.isSel = function (cur){
        return $scope.selected === cur;
    };
    $scope.setSel = function (selected) {
        $scope.selected = selected;
        $scope.Items = [];
        if ($scope.selected) {
            dataFactory.getProducts($scope.selected).success(function (data) {
                $scope.Items = data;


            }).error(function (data, status, headers, config) {
                // ...
            });

        }
    };
    if($scope.user){
        dataFactory.getLists($scope.user).success(function (data) {
            $scope.Lists = data;
        }).error(function (data, status, headers, config) {
            // ...
        });
    }



});
app.directive('itemList', function () {
    return {
        restrict: 'E',
        templateUrl: 'view2/Lists.html',
        controller:'Lists'
    };
});
