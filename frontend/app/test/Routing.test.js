/**
 * Created by lukaszurbanski on 16.03.15.
 */
it('should map routes to controllers', function() {
    module('myApp');

    inject(function($route) {

        expect($route.routes['/main'].controller).toBe('View2Ctrl');
        expect($route.routes['/main'].templateUrl).
            toEqual('view2/view2.html');

        expect($route.routes['/login'].controller).toBe('Users');
        expect($route.routes['/login'].templateUrl).
            toEqual('view1/view1.html');
        // otherwise redirect to
        expect($route.routes[null].redirectTo).toEqual('/login')
    });
});