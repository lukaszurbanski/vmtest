describe('Service : AuthService', function () {
    var  service;


    beforeEach(module('myApp'));

    beforeEach(inject(function ( _AuthService_ ) {
        // Set up the mock http service responses
        service = _AuthService_;


    }));


    it('should have an login function', function () {
        expect(angular.isFunction(service.login)).toBe(true);
    });
    it('should have an currentUser function', function () {
        expect(angular.isFunction(service.currentUser)).toBe(true);
    });
    it('should have an pushUsers function', function () {
        expect(angular.isFunction(service.pushUsers)).toBe(true);
    });
    it('should have an getUsers function', function () {
        expect(angular.isFunction(service.getUsers)).toBe(true);
    });
    it('should have an pushUser function', function () {
        expect(angular.isFunction(service.pushUser)).toBe(true);
    });

    it('should login user', function() {
        var user = { name: 'Josh Bavari', id: 1 };

        service.login(user);
        var currUser = service.currentUser();
        expect(currUser.name).toBe(user.name);
        expect(currUser.id).toBe(user.id);

    });
    it('should save all users', function() {
        var users = [{ name: 'John', id: 1 },{ name: 'Josh', id: 2 }],i;

        service.pushUsers(users);
      var result = service.getUsers();
        expect(result).toBe(users);
    });

    it('should add user to userlist',function(){
        var user = { name: 'Josh', id: 1};
        var user2 = { name: 'Bavari', id: 2};

        service.pushUser(user);

        var users = service.getUsers();

        expect(users).toContain(user);
        expect(users).not.toContain(user2);
    });

});