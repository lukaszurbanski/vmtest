describe('Factory : dataFactory', function () {
    var factory,
        httpb;


    beforeEach(module('myApp', 'ngMock'));

    beforeEach(inject(function ($httpBackend, _dataFactory_) {
        // Set up the mock http service responses
        factory = _dataFactory_;
        httpb = $httpBackend

    }));
//Bezsensowne testy
    it('should log the urlbase', function () {
        var base = factory.returnUrlBase();
        console.log('BackendBaseUrl: ' + base);
    });

    it('should have an getUsers function', function () {

        expect(angular.isFunction(factory.getUsers)).toBe(true);

    });
    it('should have an sendList function', function () {
        expect(angular.isFunction(factory.sendList)).toBe(true);
        var list = {};
        list.listname = 'name';
        list.whos = '3';
        httpb.when('GET', '../../backend/web/app_dev.php/addList/name/0/3')
            .respond(200);
        factory.sendList(list)

        httpb.flush();


    });
    it('should have an sendItem function', function () {
        expect(angular.isFunction(factory.sendItem)).toBe(true);
    });
    it('should have an delList function', function () {
        expect(angular.isFunction(factory.delList)).toBe(true);
    });
    it('should have an delProduct function', function () {
        expect(angular.isFunction(factory.delProduct)).toBe(true);
    });
    it('should have an getProducts function', function () {
        expect(angular.isFunction(factory.getProducts)).toBe(true);
    });
    it('should have an getLists function', function () {
        expect(angular.isFunction(factory.getLists)).toBe(true);
    });
    it('should have an addUser function', function () {
        expect(angular.isFunction(factory.addUser)).toBe(true);
    });
    it('should have an delUser function', function () {
        expect(angular.isFunction(factory.delUser)).toBe(true);
    });

});



