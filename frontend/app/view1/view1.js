'use strict';

app.controller('Users', function ($scope, $http, $location,$mdDialog, AuthService, dataFactory) {
    $scope.users = [];
    $scope.status = 0;
    $scope.firstuser=0;
    dataFactory.getUsers().success(function (data) {
        $scope.users = data;
        AuthService.pushUsers(data);
        if(data.length == 0){

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'view2/users.html'
            })
            
        }
        else{
            $scope.firstuser=1;

        }


    }).error(function (data, status, headers, config) {
        // ...
    });
    $scope.currentUser = AuthService.currentUser();
    $scope.checkPassword = function (pin, pin2, user) {
        if (pin === pin2) {
            $scope.status = 1;
            AuthService.login(user);

        }
        else {
            $scope.status = 0;

        }

    };
    $scope.isUndefined = function (thing) {
        return ( thing === "undefined" );
    };
    $scope.goButton = function () {
        $location.path("/main");
    };



});

