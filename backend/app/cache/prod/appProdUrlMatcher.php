<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // login_homepage
        if (0 === strpos($pathinfo, '/add') && preg_match('#^/add/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_homepage')), array (  '_controller' => 'LoginBundle\\Controller\\DefaultController::addUserAction',));
        }

        if (0 === strpos($pathinfo, '/get')) {
            // view_users
            if ($pathinfo === '/getUsers') {
                return array (  '_controller' => 'LoginBundle\\Controller\\DefaultController::getUsersAction',  '_route' => 'view_users',);
            }

            // get_lists
            if ($pathinfo === '/getLists') {
                return array (  '_controller' => 'LoginBundle\\Controller\\DefaultController::getListsAction',  '_route' => 'get_lists',);
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            // add_lists
            if (0 === strpos($pathinfo, '/addList') && preg_match('#^/addList/(?P<name>[^/]++)/(?P<count>[^/]++)/(?P<whos>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_lists')), array (  '_controller' => 'LoginBundle\\Controller\\DefaultController::addListAction',));
            }

            // homepage
            if ($pathinfo === '/app/example') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
