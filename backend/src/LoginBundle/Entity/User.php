<?php

namespace LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var string
     */
    private $usenname;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $avatarurl;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set usenname
     *
     * @param string $usenname
     * @return User
     */
    public function setUsenname($usenname)
    {
        $this->usenname = $usenname;

        return $this;
    }

    /**
     * Get usenname
     *
     * @return string 
     */
    public function getUsenname()
    {
        return $this->usenname;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set avatarurl
     *
     * @param string $avatarurl
     * @return User
     */
    public function setAvatarurl($avatarurl)
    {
        $this->avatarurl = $avatarurl;

        return $this;
    }

    /**
     * Get avatarurl
     *
     * @return string 
     */
    public function getAvatarurl()
    {
        return $this->avatarurl;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
