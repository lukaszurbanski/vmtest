<?php

namespace LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lists
 */
class Lists
{
    /**
     * @var string
     */
    private $listname;

    /**
     * @var integer
     */
    private $itemscount;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set listname
     *
     * @param string $listname
     * @return Lists
     */
    public function setListname($listname)
    {
        $this->listname = $listname;

        return $this;
    }

    /**
     * Get listname
     *
     * @return string 
     */
    public function getListname()
    {
        return $this->listname;
    }

    /**
     * Set itemscount
     *
     * @param integer $itemscount
     * @return Lists
     */
    public function setItemscount($itemscount)
    {
        $this->itemscount = $itemscount;

        return $this;
    }

    /**
     * Get itemscount
     *
     * @return integer 
     */
    public function getItemscount()
    {
        return $this->itemscount;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Lists
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $whos;


    /**
     * Set whos
     *
     * @param string $whos
     * @return Lists
     */
    public function setWhos($whos)
    {
        $this->whos = $whos;

        return $this;
    }

    /**
     * Get whos
     *
     * @return string 
     */
    public function getWhos()
    {
        return $this->whos;
    }
}
