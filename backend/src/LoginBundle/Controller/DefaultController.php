<?php

namespace LoginBundle\Controller;

use LoginBundle\Entity\User;
use LoginBundle\Entity\Products;
use LoginBundle\Entity\Lists;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;


class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        //$username = $request->request->get('username', 'default value if bar does not exist');;


    }

    public function addUserAction($name, $pin)
    {

        $User = new User();
        $User->setUsenname($name);
        $User->setPassword($pin);
        $url = $this->get('request')->get('url');
        $User->setAvatarurl($url);

        $em = $this->getDoctrine()->getManager();

        $em->persist($User);
        $em->flush();

        $response = new Response('{"id": ' . $User->getId() . '}');
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function addListAction($name, $count, $whos)
    {

        $List = new Lists();
        $List->setListname($name);
        $List->setItemscount($count);
        $List->setModified(new \DateTime("now"));
        $List->setWhos($whos);

        $em = $this->getDoctrine()->getManager();

        $em->persist($List);
        $em->flush();

        $response = new Response('{"id": ' . $List->getId() . '}');
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function addProductsAction($name, $count, $id)
    {

        $List = new Products();
        $List->setProductname($name);
        $List->setPriority($count);
        $List->setListId($id);


        $em = $this->getDoctrine()->getManager();

        $em->persist($List);
        $em->flush();

        $response = new Response('{"id": ' . $List->getId() . '}');
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function getListsAction($whos)
    {
        //$username = $request->request->get('username', 'default value if bar does not exist');;


        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT p.id, p.listname, p.itemscount
    FROM LoginBundle:Lists p
    WHERE p.whos = :whos
    OR p.whos = '*'
    "
        )
            ->setParameter('whos', $whos);

        $list = $query->getResult();
        $jsonContent = $serializer->serialize($list, 'json');

        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function getProductsAction($id)
    {
        //$username = $request->request->get('username', 'default value if bar does not exist');;


        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT p.id, p.productname, p.priority
    FROM LoginBundle:Products p
    WHERE p.listId = :listid
    OR p.listId = '*'
    "
        )
            ->setParameter('listid', $id);

        $products = $query->getResult();

        $jsonContent = $serializer->serialize($products, 'json');

        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function getUsersAction()
    {


        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()
            ->getEntityManager();

        $blogs = $em->createQueryBuilder()
            ->select('usenname')
            ->from('LoginBundle:User', 'usenname')
            ->getQuery()
            ->getResult();

        $jsonContent = $serializer->serialize($blogs, 'json');

        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function delListAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $List = $em->getRepository('LoginBundle:Lists')->find($id);
        if (!$List) {
            throw $this->createNotFoundException('No list found');
        }

        $em->remove($List);
        $em->flush();


        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('DELETE FROM LoginBundle:Products e WHERE e.listId = :id');
        $query->setParameter('id', $id);
        $query->execute();

        $response = new Response('Delated?');
        return $response;

    }

    public function delProductAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $List = $em->getRepository('LoginBundle:Products')->find($id);
        if (!$List) {
            throw $this->createNotFoundException('No list found');
        }

        $em->remove($List);
        $em->flush();

        $response = new Response('Delated?');
        return $response;

    }
    public function delUserAction($id){

        $em = $this->getDoctrine()->getEntityManager();

        $List = $em->getRepository('LoginBundle:User')->find($id);
        if (!$List) {
            throw $this->createNotFoundException('No list found');
        }

        $em->remove($List);
        $em->flush();

        $response = new Response('Delated?');
        return $response;

    }
}
